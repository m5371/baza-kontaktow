from flask_login import UserMixin
from . import db


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))


class Contact(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    imie = db.Column(db.VARCHAR(45))
    nazwisko = db.Column(db.VARCHAR(45))
    adres = db.Column(db.VARCHAR(1000))
    telefon = db.Column(db.VARCHAR(12))
    email = db.Column(db.VARCHAR(100))


class helpdesk(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    imie = db.Column(db.VARCHAR)
    email = db.Column(db.VARCHAR)
    country = db.Column(db.VARCHAR)
    subject = db.Column(db.VARCHAR)
    content = db.Column(db.VARCHAR)
