from flask import Blueprint, render_template, flash
from flask_login import login_required, current_user
from . import db
from .models import Contact
import sqlalchemy

engin = sqlalchemy.create_engine('sqlite:///db.sqlite')

main = Blueprint('main', __name__)


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/kontakty')
@login_required
def kontakty():
    data = DB.pobierz()

    if type(data) != sqlalchemy.engine.cursor.LegacyCursorResult:
        flash("Brak kontaktów")

    return render_template('kontakty.html',
                           naglowki=[' ID ', ' Imie ', ' Nazwisko ', ' Adres ', ' Telefon ', ' E-mail '],
                           dane=data)


@main.route('/kontakty-zarzadzaj')
def kontakty_zarzadzaj():
    data = DB.pobierz()

    if type(data) != sqlalchemy.engine.cursor.LegacyCursorResult:
        flash("Brak kontaktów")

    return render_template('kontakty-menu.html',
                           naglowki=[' ID ', ' Imie ', ' Nazwisko ', ' Adres ', ' Telefon ', ' E-mail '],
                           dane=data)


@main.route('/helpdesk')
def helpdesk():
    return render_template('helpdesk.html')


@main.route('/helpdesk_zarzadzaj')
@login_required
def helpdesk_zarzadzaj():
    data = DB.pobierz_helpdesk()

    if type(data) != sqlalchemy.engine.cursor.LegacyCursorResult:
        flash("Brak Zgłoszeń")

    return render_template('helpdesk_zarzadzaj.html',
                           naglowki=[' ID ', ' Imie ', ' Email ', ' Kraj ', ' Temat ', ' Opis ', ' Akcje '],
                           dane=data)


class DB:
    def pobierz(self):
        data = engin.execute('Select * from Contact')
        return data

    def pobierz_po_id(self, id):
        data = engin.execute(f'Select * from Contact where id={id}')
        return data

    def pobierz_helpdesk(self):
        data = engin.execute('select * from helpdesk')
        return data

    def pobierz_helpdesk_id(self, id):
        data = engin.execute(f'Select * from helpdesk where id={id}')
        return data


DB = DB()
