from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from .models import User, Contact, helpdesk
from flask_login import login_user
from . import db
from flask_login import login_user, logout_user, login_required
from .main import DB, engin

auth = Blueprint('auth', __name__)


@auth.route('/zaloguj', methods=['POST'])
def zaloguj_post():
    email = request.form.get('email')
    password = request.form.get('haslo')
    remember = True if request.form.get('zap') else False

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        flash('Sprawdz swoje dane i spróbuj ponownie')
        return redirect(url_for('auth.zaloguj'))

    login_user(user, remember=remember)
    return redirect(url_for('main.kontakty'))


@auth.route('/zaloguj')
def zaloguj():
    return render_template('login.html')


@auth.route('/zarejestruj', methods=['POST'])
def zarejestruj_post():
    email = request.form.get('email')
    name = request.form.get('imie')
    password = request.form.get('haslo')

    user = User.query.filter_by(
        email=email).first()  # if this returns a user, then the email already exists in database

    if user:
        flash('Email już istnieje')
        return redirect(url_for('auth.zarejestruj'))

    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))

    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.zaloguj'))


@auth.route('/zarejestruj')
def zarejestruj():
    return render_template('singup.html')


@auth.route('/wyloguj')
def wyloguj():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/dodaj')
@login_required
def dodaj():
    return render_template('dodaj.html')


@auth.route('/dodaj', methods=['POST'])
@login_required
def dodaj_post():
    imie = request.form.get('imie')
    nazwisko = request.form.get('nazwisko')
    adres = request.form.get('adres')
    telefon = request.form.get('telefon')
    email = request.form.get('email')

    new_contact = Contact(imie=imie, nazwisko=nazwisko, adres=adres, telefon=telefon, email=email)

    db.session.add(new_contact)
    db.session.commit()

    return redirect(url_for('main.kontakty'))


@auth.route('/help_send', methods=['POST'])
def help_send():
    imie = request.form.get('imie')
    email = request.form.get('email')
    country = request.form.get('country')
    subject = request.form.get('subject')
    content = request.form.get('content')

    new_help = helpdesk(imie=imie, email=email, country=country, subject=subject, content=content)
    db.session.add(new_help)
    db.session.commit()
    flash(f"Wysłano zgłoszenie o nr: {new_help.get_id()}")

    return redirect(url_for('main.helpdesk'))


@auth.route('/delete/<name>')
def delete(name):
    data = Contact.query.filter_by(id=name).first()

    db.session.delete(data)
    db.session.commit()

    return redirect(url_for('main.kontakty'))


@auth.route('/edit/<name>')
def edit(name):
    lista = []
    data = DB.pobierz_po_id(name)

    for i in data:
        for x in i:
            lista.append(x)

    return render_template('edit.html', data=lista)


@auth.route('/edit/<name>', methods=['POST'])
def edit_post(name):
    imie = request.form.get('imie')
    nazwisko = request.form.get('nazwisko')
    adres = request.form.get('adres')
    telefon = request.form.get('telefon')
    email = request.form.get('email')

    engin.execute(
        f'Update Contact set imie="{imie}", nazwisko="{nazwisko}", adres="{adres}", telefon="{telefon}", email="{email}" where id="{name}"')

    return redirect(url_for('main.kontakty'))
